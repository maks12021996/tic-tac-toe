let sign = "X";
let count_round = 0;
let caption = Array(9).fill("");
let game_mode = true;
let cells = Array.from(document.getElementsByClassName("cell"));
let title = document.getElementById("title");
set_title_move = () => {
  if (count_round > 8) {
    title.innerHTML = "НІЧИЯ!";
  } else {
    title.innerHTML = "НАСТУПНИЙ ХІД: " + sign;
    title.style.color = sign === "X" ? "blue" : "green";
    count_round += 1;
  }
};
set_title_move();

move = (i) => {
  if (cells[i].innerHTML === "" && game_mode) {
    cells[i].style.color = sign === "X" ? "blue" : "green";
    cells[i].innerHTML = sign;
    caption[i] = sign;
    let winner = check_winner();
    if (winner) {
      title.innerHTML = "ПЕРЕМОГА " + sign;
      game_mode = false;
    } else {
      sign = sign === "X" ? "O" : "X";
      set_title_move();
    }
  }
};

check_winner = () => {
  if (
    (caption[0] !== "" &&
      caption[0] === caption[1] &&
      caption[1] === caption[2]) ||
    (caption[3] !== "" &&
      caption[3] === caption[4] &&
      caption[4] === caption[5]) ||
    (caption[6] !== "" &&
      caption[6] === caption[7] &&
      caption[7] === caption[8]) ||
    (caption[0] !== "" &&
      caption[0] === caption[3] &&
      caption[3] === caption[6]) ||
    (caption[1] !== "" &&
      caption[1] === caption[4] &&
      caption[4] === caption[7]) ||
    (caption[2] !== "" &&
      caption[2] === caption[5] &&
      caption[5] === caption[8]) ||
    (caption[0] !== "" &&
      caption[0] === caption[4] &&
      caption[4] === caption[8]) ||
    (caption[2] !== "" &&
      caption[2] === caption[4] &&
      caption[4] === caption[6])
  ) {
    return true;
  }
  return false;
};

for (let i = 0; i < cells.length; i++) {
  cells[i].addEventListener("click", () => {
    move(i);
  });
}
